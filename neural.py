import tensorflow as tf
from tensorflow import keras
from preprocessor import readInput as readInput
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def splitSets(portion, data_set):
    portion = 1/portion
    half = int(len(data_set)//(portion))
    return data_set[:half], data_set[half:]

def runNeuralNet():
    reviews, wordToIndex = readInput()

    #gets the size of the longest sentence
    longestSentenceSize = len(max([x for (x,y) in reviews], key=len))

    #add padding reserverd word to dic
    wordToIndex['<PADDING>'] = len(wordToIndex)
    vocabulary_size = len(wordToIndex)

    print('longest sentence size', longestSentenceSize)

    #splits input into validation and training data
    train_set, validation_set = splitSets(0.95, reviews)

    train_data   = [x for (x,y) in train_set]
    train_labels = [y for (x,y) in train_set]

    validation_data   = [x for (x,y) in validation_set]
    validation_labels = [y for (x,y) in validation_set]

    #pad the data
    train_data = keras.preprocessing.sequence.pad_sequences(train_data, value=wordToIndex['<PADDING>'], padding='post', maxlen=longestSentenceSize)
    validation_data = keras.preprocessing.sequence.pad_sequences(validation_data, value=wordToIndex['<PADDING>'], padding='post', maxlen=longestSentenceSize)

    model = keras.Sequential()
    model.add(keras.layers.Embedding(vocabulary_size,3000))
    model.add(keras.layers.GlobalAveragePooling1D())
    model.add(keras.layers.Dense(500, activation = tf.nn.relu))
    model.add(keras.layers.Dropout(0.9))
    model.add(keras.layers.Dense(500, activation = tf.nn.relu))
    model.add(keras.layers.Dropout(0.8))
    model.add(keras.layers.Dense(1, activation = tf.nn.sigmoid))
    model.compile(optimizer='adam',loss = 'binary_crossentropy', metrics = ['acc'])
    #model.evaluate(x=train_data, y=train_labels, batch_size=512, verbose=1)
    history = model.fit(train_data, train_labels, epochs=40, batch_size=256, validation_data=(validation_data, validation_labels), verbose=1, use_multiprocessing=True)

    model.save('my_model.hd5')

    model.summary()

    plt.figure(1)

    # summarize history for accuracy
    plt.subplot(211)
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')

    # summarize history for loss
    plt.subplot(212)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('output.png')


runNeuralNet()

