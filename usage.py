import tensorflow as tf
from preprocessor import readInput as readInput
from tensorflow import keras

reviews, wordToIndex = readInput()

#gets the size of the longest sentence
longestSentenceSize = len(max([x for (x,y) in reviews], key=len))

#add padding reserverd word to dic
wordToIndex['<PADDING>'] = len(wordToIndex)
vocabulary_size = len(wordToIndex)

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
model = tf.keras.models.load_model(filepath='my_model.hd5')

while(True):
    user_review = input("Input your review: ")

    if user_review == 'exit':
        exit()

    word2IndexInput = []
    for word in user_review.split():
        pros_word = ''.join(e for e in word if (e.isalnum() or e.isspace()))
        pros_word = pros_word.lower()
        if pros_word in wordToIndex:
            word2IndexInput.append(wordToIndex[pros_word])
        else:
            print("Skipping ", pros_word)

    word2IndexInput = keras.preprocessing.sequence.pad_sequences([word2IndexInput], value=wordToIndex['<PADDING>'], padding='post', maxlen=longestSentenceSize)
    word2IndexInput = word2IndexInput[0]
    #print(word2IndexInput)


    result = model.predict(x=[[word2IndexInput]], verbose=0)
    positivity=result[0]
    positivity=positivity[0]
    print('Positivity= ', round(positivity,2))
    print('Negativity= ', round(1 - positivity, 2))
    print('\n\n\n')

