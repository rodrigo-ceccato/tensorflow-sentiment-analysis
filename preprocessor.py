import random

def readInput():
    #fix our seed to keep order the same
    random.seed(0)

    globalDic = {}

    reviewInputs  = ['negative.txt', 'positive.txt']
    processedRevs =  []

    #reads the two sets of reviews
    for inputFile in reviewInputs:
        with open(inputFile) as f:
            for line in f.readlines():
                #joins only if alpha numeric or space
                processedLine = ''.join(e for e in line if (e.isalnum() or e.isspace()))

                #split around space
                processedLine = processedLine.split()

                #get an index for each word
                for word in processedLine:
                    if word not in globalDic:
                        index = len(globalDic)
                        globalDic[word] = index

                #get an review line using the index numbers
                indexedByNumberReview = [globalDic[item] for item in processedLine]

                #humor = 1 if positive
                sentiment = 1
                if inputFile == 'negative.txt':
                    sentiment = 0

                #processed input tuple:
                #([numbers representing sentences], 1/0)
                reviewTuple = (indexedByNumberReview, sentiment)

                #saves processed tuple
                processedRevs.append(reviewTuple)

    #shufle the output befor returning
    random.shuffle(processedRevs)

    return processedRevs, globalDic

